# Extra Software Installed on Linux-on-USB.


## Ubuntu

### VLC

One of the most widely used media player.
Command name: `vlc`.


### Gimp

Open source drawing program similar to Windows Paint.
Command name: `gimp`.


### Inkscape

Open-source vector graphics program similar to Adobe Illustrator.
Command name: `inkscape`.


### Visual Studio Code

Programmer's editor from Microsoft. Command name: `code`.


### Google Chrome

Web browser by Google. Many people prefer Chrome to Firefox, 
which is included in most Linux distribution by default.
Command name: `google-chrome`.



## Linux Mint

VLC and Gimp come with the default installation.
The rest is the same as those installed on Ubuntu.



## Fedora

The same as Ubuntu and Linux Mint.



